+++
date = "2016-04-20T00:00:00"
draft = false
title = "about"
section_id = 0
weight = 0
+++

## Biography

Mike Jacobi is a Sr. Software Engineer at Linden Lab where he is currently working on a web infrastructure team.  He likes to tinker with new software tech and prototype app ideas.  When not coding, he likes listening to history podcasts (Dan Carlin!), playing video games (Rocket League!) and cards (Spades!), and working on house projects. 
