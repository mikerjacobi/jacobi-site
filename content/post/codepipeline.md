+++
date = "2017-02-24T07:48:28Z"
tags = []
title = "AWS CodePipeline"

+++

I started using AWS CodePipeline to automate deploys of my projects to dedicated EC2 instances.  Here is a video explaining how to set up one of these pipelines.

<iframe height="300" src="https://www.youtube.com/embed/TUFZGJMCBU0" frameborder="0" allowfullscreen></iframe>
