+++
date = "2016-01-14T05:16:34Z"
tags = ["hugo"]
title = "Basics of Hugo"

+++

I just set up a hugo blog, and am recording the useful commands here.

#### Creating a New Post:

You can create different types of content, which the theme must support.  For example, this Academic theme supports `type = ["post", "publication", "project"]`

```
jacobi@licobra ~/site $ hugo new <type>/new-post.md
/home/jacobi/site/content/post/new-post.md created
```

#### Changing the Theme:
 * Browse the [themes site](http://themes.gohugo.io/)
 * Click the github link
 * Clone the github page into `<hugo root>/themes/<theme name>`
 * Modify `config.toml` to be `<theme name>`

#### Deploy the Hugo App
 * Setup a static web server pointing at `<site directory>`
 * from `<hugo root>`, run `hugo`
 * copy the `<hugo root>/public` directory to `<site directory>`
  * `cp -rT public /var/www/blog` 
