+++
client_name = ""
date = "2011-01-01T04:48:47Z"
external_link = ""
img = ""
img_preview = ""
summary = ""
tags = ["C#","XNA"]
title = "Educational First Person Shooter"

+++
This project is a first person shooter game with an educational spin. The idea is that you navigate a maze, fight enemies, and collect treasure. The treasure comes in the form of "knowledge", or simple paragraphs of information with common themes across levels. At the end of each level, there is a multiple choice quiz that covers the information from that level. The score on the quiz results in receiving stat points to allocate towards your character for power-ups.

This game was made using C# and XNA. It was team project of three. I was primarily responsible for the object to object collision detection system, the knowledge/treasure system, and the character/stat point system.

<iframe width="300" height="300" src="https://www.youtube.com/embed/Fo3n1K6CHhc"  allowfullscreen></iframe>

[Source code](https://code.google.com/p/csci427-educational-fps/source/browse/#svn%2Ftrunk) -
[Executable and Installation Instructions](https://www.mediafire.com/folder/eh8lcd5u85y23/)

