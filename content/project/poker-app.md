+++
date = "2016-05-29T05:25:20Z"
external_link = ""
image = ""
math = false
summary = ""
tags = ["poker", "golang", "javascript"]
title = "Poker App"
+++

[Play Here](http://jacobra.com:8004)

[Poker App Write Up](http://jacobra.com/post/poker-app/)

[Source Code](https://github.com/mikerjacobi/poker)

<iframe width="560" height="315" src="https://www.youtube.com/embed/6bH2E27pQP0" frameborder="0" allowfullscreen></iframe>
