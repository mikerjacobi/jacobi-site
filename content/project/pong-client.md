+++
client_name = ""
date = "2014-05-25T04:52:57Z"
external_link = ""
img = ""
img_preview = ""
summary = ""
tags = ["android"]
title = "Android Pong Client"

+++

Pong Client is an Android app that makes tracking ping pong data easy.  Each time a point is scored, you click the corresponding player to record it.  For each game, this app records the final score, the time that each point was scored, the order that each point was scored, and the winner of the game.  That data is visualized in the [pong graphs](http://pongapp.s3-website-us-west-2.amazonaws.com/#/) app.

![pong client](/img/Pongcli-downsized.png)
