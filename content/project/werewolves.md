+++
client_name = ""
date = "2012-07-22T04:52:53Z"
external_link = ""
img = ""
img_preview = ""
summary = ""
tags = ["python"]
title = "Virtual Werewolves"

+++

Virtual Werewolves is a commandline multiplayer game designed to teach Cybersecurity.  Also known as Mafia.  This was a project that I created as a teaching assistant in grad school, written in Python.  It was made as a lab project for the undergraduates that were taking the Cybersecurity class.  See my [blog post](/post/wwblog) for more info.

The slides below were for the final presentation that I delivered after finishing a version one of the project.

<iframe src="https://www.slideshare.net/slideshow/embed_code/18368385" height="300" width="300" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" allowfullscreen></iframe>

[Werewolves home page](http://werewolves.cs.unm.edu/)
[Source](https://bitbucket.org/mjacobi/virtual-werewolves)


