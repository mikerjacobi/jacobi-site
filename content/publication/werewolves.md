+++
abstract = "Virtual Werewolves is a Linux based, multiplayer game designed to teach Cybersecurity. Players must exploit inference channels in the Linux kernel to identify other's identities and conceal their own. We published a paper on the educational merits of Virtual Werewolves at CSET in August 2012."
authors = ["Roya Ensafi", "Mike Jacobi", "Jedidiah R. Crandall"]
date = "2016-05-12T04:43:21Z"
publication = "CSET '12"
title = "Students Who Don’t Understand Information Flow Should be Eaten: An Experience Paper"
url_code = "https://bitbucket.org/mjacobi/virtual-werewolves/overview"
url_dataset = ""
url_image = ""
url_pdf = "http://www.cs.unm.edu/~crandall/CSET12.pdf"
url_project = "http://werewolves.cs.unm.edu/"
url_slides = ""
url_video = ""

+++

